//
//  TableViewCell.swift
//  TestProjectITS
//
//  Created by Artem Manyshev on 11.07.2020.
//  Copyright © 2020 Artem Manyshev. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var containerButton: UIView!
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var ageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerButton.layer.cornerRadius = 5.0
        avatarImageView.layer.cornerRadius = avatarImageView.bounds.height / 2
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func setup(with model: CellsModel) {
        nameLabel.text = model.name
        ageLabel.text = model.age
        switch model.gender {
        case .female:
            avatarImageView.image = UIImage(named: Constants.ImageNames.womanAvatar.rawValue)
            avatarImageView.backgroundColor = UIColor.systemPink.withAlphaComponent(0.1)
        case .male:
            avatarImageView.image = UIImage(named: Constants.ImageNames.manAvatar.rawValue)
            avatarImageView.backgroundColor = UIColor.blue.withAlphaComponent(0.1)
        }
    }
}
