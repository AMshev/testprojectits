//
//  TableViewController.swift
//  TestProjectITS
//
//  Created by Artem Manyshev on 11.07.2020.
//  Copyright © 2020 Artem Manyshev. All rights reserved.
//

import UIKit

class TableViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var switchGenderBarButton: UIBarButtonItem!
    
    private let downloadDatalink = Constants.DownloadLinks.personsDataUrl.rawValue
    private var allPersonsData: [PersonsData] = []
    private var copyOfAllPersonsData: [PersonsData]?
    private var selectedPersonsData: PersonsData?
    private var shownGender: PersonsGender = .mix
    private var sortAscending = true
    private let concurrentQueue = DispatchQueue(label: "concurrentTableView", qos: .userInitiated,
                                                attributes: .concurrent)
    
    private let downloadProgressView: UIProgressView = {
        let progressView = UIProgressView()
        progressView.progressTintColor = .black
        return progressView
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        label.text = "Table"
        return label
    }()
    
    private let refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.refreshControl = refreshControl
        tableView.contentInset = UIEdgeInsets(top: 10.0, left: 0, bottom: 0, right: 0)
        tableView.refreshControl?.beginRefreshing()
        concurrentQueue.async {
            PersonsDataHandler.shared.getPersonsData (from: self.downloadDatalink) { [weak self] (progress, persons) in
                self?.processData(progress: progress, persons: persons)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    @objc private func refreshData() {
        PersonsDataHandler.shared.reloadPersonsData(from: downloadDatalink) { [weak self] (progress, persons) in
            guard let self = self else { return }
            self.processData(progress: progress, persons: persons)
        }
        shownGender = .mix
        DispatchQueue.main.async {
            self.switchGenderBarButton.image = UIImage(named: Constants.ImageNames.mix.rawValue)
        }
    }
    
    private func processData(progress: Double?, persons: [PersonsData]?) {
        if let progress = progress {
            DispatchQueue.main.async {
                self.navigationItem.titleView = self.downloadProgressView
                self.downloadProgressView.setProgress(Float(progress), animated: true)
            }
        }
        if let persons = persons {
            allPersonsData = persons
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.tableView.refreshControl?.endRefreshing()
                self.downloadProgressView.isHidden = true
                self.navigationItem.titleView = self.titleLabel
            }
        }
    }
    
    @IBAction private func sortButtonTapped(_ sender: Any) {
        concurrentQueue.async { [weak self] in
            guard let self = self else { return }
            self.allPersonsData.sort(by: {
                if self.sortAscending {
                    return $0.age < $1.age
                } else {
                    return $0.age > $1.age
                }
            })
            self.sortAscending = !self.sortAscending
            DispatchQueue.main.async {
                self.tableView.reloadSections(IndexSet(integersIn: 0...0), with: .automatic)
            }
        }
    }
    
    @IBAction private func switchGenderBarButtonTapped(_ sender: Any) {
        concurrentQueue.async { [weak self] in
            guard let self = self else { return }
            if self.copyOfAllPersonsData == nil {
                self.copyOfAllPersonsData = self.allPersonsData
            }
            guard let copyOfPersonsData = self.copyOfAllPersonsData else { return }
            switch self.shownGender {
            case .man:
                self.allPersonsData = copyOfPersonsData.compactMap({ $0.gender == .female ? $0 : nil})
                self.shownGender = .woman
                DispatchQueue.main.async {
                    self.switchGenderBarButton.image = UIImage(named: Constants.ImageNames.woman.rawValue)
                }
            case .woman:
                self.allPersonsData = copyOfPersonsData
                self.shownGender = .mix
                DispatchQueue.main.async {
                    self.switchGenderBarButton.image = UIImage(named: Constants.ImageNames.mix.rawValue)
                }
            case .mix:
                self.allPersonsData = copyOfPersonsData.compactMap({$0.gender == .male ? $0 : nil})
                self.shownGender = .man
                DispatchQueue.main.async {
                    self.switchGenderBarButton.image = UIImage(named: Constants.ImageNames.man.rawValue)
                }
            }
            DispatchQueue.main.async {
                self.tableView.reloadSections(IndexSet(integersIn: 0...0), with: .automatic)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Identifiers.detailPage.rawValue {
            let destinationVC = segue.destination as? DetailPageViewController
            destinationVC?.personsData = selectedPersonsData
        }
    }
}

extension TableViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        allPersonsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Identifiers.tableViewCell.rawValue, for: indexPath)
            as? TableViewCell else { return UITableViewCell() }
        let model = CellsModel(personData: allPersonsData[indexPath.row])
        cell.setup(with: model)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedPersonsData = allPersonsData[indexPath.row]
        performSegue(withIdentifier: Constants.Identifiers.detailPage.rawValue, sender: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        60.0
    }
}
