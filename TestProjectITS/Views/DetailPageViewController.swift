//
//  DetailPageViewController.swift
//  TestProjectITS
//
//  Created by Artem Manyshev on 11.07.2020.
//  Copyright © 2020 Artem Manyshev. All rights reserved.
//

import UIKit

class DetailPageViewController: UIViewController {
    
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var ageLabel: UILabel!
    @IBOutlet private weak var balanceLabel: UILabel!
    @IBOutlet private weak var eyeColorLabel: UILabel!
    @IBOutlet private weak var favoriteFruitLabel: UILabel!
    
    public var personsData: PersonsData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let personsData = personsData else { return }
        let model = DetailPageModel(personData: personsData)
        setupViews(with: model)
        avatarImageView.layer.cornerRadius = avatarImageView.bounds.height / 2
    }
    
    private func setupViews(with model: DetailPageModel) {
        switch model.gender {
        case .female:
            avatarImageView.image = UIImage(named: Constants.ImageNames.womanAvatar.rawValue)
            avatarImageView.backgroundColor = UIColor.systemPink.withAlphaComponent(0.1)
        case .male:
            avatarImageView.image = UIImage(named: Constants.ImageNames.manAvatar.rawValue)
            avatarImageView.backgroundColor = UIColor.blue.withAlphaComponent(0.1)
        }
        navigationItem.title = model.name
        ageLabel.text = model.age
        balanceLabel.text = model.balance
        eyeColorLabel.text = model.eyeColor
        favoriteFruitLabel.text = model.favoriteFruit
    }
}
