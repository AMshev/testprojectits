//
//  CollectionViewCell.swift
//  TestProjectITS
//
//  Created by Artem Manyshev on 11.07.2020.
//  Copyright © 2020 Artem Manyshev. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var ageLabel: UILabel!
    
    private let avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 10.0
        return imageView
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .white
        layer.cornerRadius = 10.0
        addSubview(avatarImageView)
        NSLayoutConstraint.activate([
            avatarImageView.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            avatarImageView.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
    
    public func setup(with model: CellsModel) {
        nameLabel.text = model.name
        ageLabel.text = model.age
        switch model.gender {
        case .female:
            avatarImageView.image = UIImage(named: Constants.ImageNames.womanAvatar.rawValue)
            avatarImageView.backgroundColor = UIColor.systemPink.withAlphaComponent(0.1)
        case .male:
            avatarImageView.image = UIImage(named: Constants.ImageNames.manAvatar.rawValue)
            avatarImageView.backgroundColor = UIColor.blue.withAlphaComponent(0.1)
        }
    }
}
