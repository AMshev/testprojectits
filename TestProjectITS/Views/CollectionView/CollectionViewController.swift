//
//  CollectionViewController.swift
//  TestProjectITS
//
//  Created by Artem Manyshev on 11.07.2020.
//  Copyright © 2020 Artem Manyshev. All rights reserved.
//

import UIKit

class CollectionViewController: UIViewController {
    
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var collectionViewLayout: UICollectionViewFlowLayout!
    @IBOutlet private weak var switchGenderBarButton: UIBarButtonItem!
    
    private let downloadDataLink = Constants.DownloadLinks.personsDataUrl.rawValue
    private var allPersonsData: [PersonsData] = []
    private var copyOfAllPersonsData: [PersonsData]?
    private var selectedPersonData: PersonsData?
    private var shownGender: PersonsGender = .mix
    private var sortAscending = true
    private let concurrentQueue = DispatchQueue(label: "concurrentCollectionView",
                                                qos: .userInitiated, attributes: .concurrent)
    
    private let downloadProgressView: UIProgressView = {
        let progressView = UIProgressView()
        progressView.progressTintColor = .black
        return progressView
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        label.text = "Collection"
        return label
    }()
    
    private let refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.bounds = CGRect(x: -20, y: refreshControl.bounds.origin.y,
                                       width: refreshControl.bounds.width,
                                       height: refreshControl.bounds.height)
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.refreshControl = refreshControl
        collectionView.refreshControl?.beginRefreshing()
        collectionView.contentInset = UIEdgeInsets(top: 20.0, left: 20.0, bottom: 10.0, right: 20.0)
        collectionViewLayout.minimumLineSpacing = 10.0
        concurrentQueue.async {
            PersonsDataHandler.shared.getPersonsData(from: self.downloadDataLink) { [weak self] (progress, persons) in
                self?.processData(progress: progress, persons: persons)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    @objc private func refreshData() {
        PersonsDataHandler.shared.reloadPersonsData(from: downloadDataLink) { [weak self] (progress, persons) in
            guard let self = self else { return }
            self.processData(progress: progress, persons: persons)
        }
        shownGender = .mix
        DispatchQueue.main.async {
            self.switchGenderBarButton.image = UIImage(named: Constants.ImageNames.mix.rawValue)
        }
    }
    
    private func processData(progress: Double?, persons: [PersonsData]?) {
        if let progress = progress {
            DispatchQueue.main.async {
                self.navigationItem.titleView = self.downloadProgressView
                self.downloadProgressView.setProgress(Float(progress), animated: true)
            }
        }
        if let persons = persons {
            allPersonsData = persons
            DispatchQueue.main.async {
                self.collectionView.reloadData()
                self.collectionView.refreshControl?.endRefreshing()
                self.downloadProgressView.isHidden = true
                self.navigationItem.titleView = self.titleLabel
            }
        }
    }
    
    @IBAction private func sortButtonTapped(_ sender: Any) {
        concurrentQueue.async { [weak self] in
            guard let self = self else { return }
            self.allPersonsData.sort(by: {
                if self.sortAscending {
                    return $0.age < $1.age
                } else {
                    return $0.age > $1.age
                }
            })
            self.sortAscending = !self.sortAscending
            DispatchQueue.main.async {
                self.collectionView.performBatchUpdates({ self.collectionView.reloadSections(IndexSet(integersIn: 0...0))
                }, completion: nil)
            }
        }
    }
    
    @IBAction private func switchGenderBarButtonTapped(_ sender: Any) {
        concurrentQueue.async { [weak self] in
            guard let self = self else { return }
            if self.copyOfAllPersonsData == nil {
                self.copyOfAllPersonsData = self.allPersonsData
            }
            guard let copyOfPersonsData = self.copyOfAllPersonsData else { return }
            switch self.shownGender {
            case .man:
                self.allPersonsData = copyOfPersonsData.compactMap({$0.gender == .female ? $0 : nil})
                self.shownGender = .woman
                DispatchQueue.main.async {
                    self.switchGenderBarButton.image = UIImage(named: Constants.ImageNames.woman.rawValue)
                }
            case .woman:
                self.allPersonsData = copyOfPersonsData
                self.shownGender = .mix
                DispatchQueue.main.async {
                    self.switchGenderBarButton.image = UIImage(named: Constants.ImageNames.mix.rawValue)
                }
            case .mix:
                self.allPersonsData = copyOfPersonsData.compactMap({$0.gender == .male ? $0 : nil})
                self.shownGender = .man
                DispatchQueue.main.async {
                    self.switchGenderBarButton.image = UIImage(named: Constants.ImageNames.man.rawValue)
                }
            }
            DispatchQueue.main.async {
                self.collectionView.performBatchUpdates({
                    self.collectionView.reloadSections(IndexSet(integersIn: 0...0))
                }, completion: nil)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Identifiers.detailPage.rawValue {
            let destinationVC = segue.destination as? DetailPageViewController
            destinationVC?.personsData = selectedPersonData
        }
    }
}

extension CollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        allPersonsData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.Identifiers.collectionViewCell.rawValue, for: indexPath)
            as? CollectionViewCell else { return UICollectionViewCell() }
        let model = CellsModel(personData: allPersonsData[indexPath.item])
        cell.setup(with: model)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedPersonData = allPersonsData[indexPath.item]
        performSegue(withIdentifier: Constants.Identifiers.detailPage.rawValue, sender: nil)
    }
}

extension CollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: (UIScreen.main.bounds.width - 51.0) / 2, height: UIScreen.main.bounds.height / 3)
    }
}
