//
//  ViewController.swift
//  TestProjectITS
//
//  Created by Artem Manyshev on 10.07.2020.
//  Copyright © 2020 Artem Manyshev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet private weak var tableViewButton: UIButton!
    @IBOutlet private weak var collectionViewButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewButton.layer.cornerRadius = 5.0
        collectionViewButton.layer.cornerRadius = 5.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction private func tableViewButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: Constants.Identifiers.tableView.rawValue, sender: nil)
    }
    @IBAction private func collectionViewButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: Constants.Identifiers.collectionView.rawValue, sender: nil)
    }
}
