//
//  DetailPageModel.swift
//  TestProjectITS
//
//  Created by Artem Manyshev on 12.07.2020.
//  Copyright © 2020 Artem Manyshev. All rights reserved.
//

import Foundation

struct DetailPageModel {
    
    let name: String
    let age: String
    let balance: String
    let eyeColor: String
    let favoriteFruit: String
    let gender: Gender
    
    init(personData: PersonsData) {
        name = personData.name
        age = String(personData.age)
        balance = personData.balance
        eyeColor = personData.eyeColor
        favoriteFruit = personData.favoriteFruit
        gender = personData.gender
    }
}
