//
//  CellsModel.swift
//  TestProjectITS
//
//  Created by Artem Manyshev on 11.07.2020.
//  Copyright © 2020 Artem Manyshev. All rights reserved.
//

import Foundation

struct CellsModel {
    let name: String
    let age: String
    let gender: Gender
    
    init(personData: PersonsData) {
        name = personData.name
        age = String(personData.age)
        gender = personData.gender
    }
}
