//
//  NetworkHandler.swift
//  TestProjectITS
//
//  Created by Artem Manyshev on 10.07.2020.
//  Copyright © 2020 Artem Manyshev. All rights reserved.
//

import Foundation
import Alamofire

class NetworkHandler {
    
    static public let shared = NetworkHandler()
    private let concurrentQueue = DispatchQueue(label: "concurrentNetwork", qos: .background,
                                                attributes: .concurrent)
    
    private init() {}
    
    public func fetchData(from url: String, callback: @escaping (Double?, Data?) -> Void) {
        AF.request(url)
            .downloadProgress(queue: concurrentQueue, closure: { callback($0.fractionCompleted, nil) })
            .response(queue: concurrentQueue) { callback(nil, $0.data) }
    }
}
