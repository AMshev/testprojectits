//
//  PersonsDataHandler.swift
//  TestProjectITS
//
//  Created by Artem Manyshev on 13.07.2020.
//  Copyright © 2020 Artem Manyshev. All rights reserved.
//

import Foundation

class PersonsDataHandler {
    
    static public let shared = PersonsDataHandler()
    private var persons: [PersonsData]?
    
    private init() {}
    
    private func loadPersonsData(from link: String, callback: @escaping (Double?, [PersonsData]?) -> Void) {
        DataHandler.shared.getDataAndProgress(from: link, callback: callback)
    }
    
    public func getPersonsData(from link: String, callback: @escaping (Double?, [PersonsData]?) -> Void) {
        if persons == nil {
            loadPersonsData(from: link) { [weak self] (progress, personsData) in
                callback(progress, nil)
                self?.persons = personsData
                callback(nil, self?.persons)
            }
        } else {
            callback(nil, self.persons)
        }
    }
    
    public func reloadPersonsData(from link: String, callback: @escaping (Double?, [PersonsData]?) -> Void) {
        persons = nil
        getPersonsData(from: link, callback: callback)
    }
}

struct PersonsData: Codable {
    let index, age: Int
    let _id, name, picture, balance, guid, eyeColor, favoriteFruit: String
    let gender: Gender
    let isActive: Bool
}

enum Gender: String, Codable {
    case female
    case male
}
