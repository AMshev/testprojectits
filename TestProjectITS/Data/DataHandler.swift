//
//  DataHandler.swift
//  TestProjectITS
//
//  Created by Artem Manyshev on 11.07.2020.
//  Copyright © 2020 Artem Manyshev. All rights reserved.
//

import Foundation

class DataHandler {
    
    static public let shared = DataHandler()
    
    private init() {}
    
    public func getDataAndProgress<T: Codable>(from link: String, callback: @escaping (Double?, T?) -> Void) {
        NetworkHandler.shared.fetchData(from: link) { [weak self] (progress, data) in
            callback(progress, nil)
            do {
                guard let data = data else { return }
                let parsedData: T? = try self?.parseData(data: data)
                callback(nil, parsedData)
            } catch { print(error.localizedDescription) }
        }
    }
    
    private func parseData<T: Codable>(data: Data) throws -> T {
        try JSONDecoder().decode(T.self, from: data)
    }
}
