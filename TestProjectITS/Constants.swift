//
//  Constants.swift
//  TestProjectITS
//
//  Created by Artem Manyshev on 11.07.2020.
//  Copyright © 2020 Artem Manyshev. All rights reserved.
//

import Foundation

enum Constants {
    enum Identifiers: String {
        case tableView
        case collectionView
        case detailPage
        case collectionViewCell
        case tableViewCell
    }
    
    enum ImageNames: String {
        case womanAvatar
        case manAvatar
        case woman
        case man
        case mix
    }
    
    enum DownloadLinks: String {
        case personsDataUrl = "https://www.json-generator.com/api/json/get/clyWAUbaxu?indent=2"
    }
}

enum PersonsGender {
    case man
    case woman
    case mix
}
